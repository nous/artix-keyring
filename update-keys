#!/bin/bash

set -e

export LANG=C

TMPDIR=$(mktemp -d)
trap "rm -rf '${TMPDIR}'" EXIT

KEYSERVER='hkps://pgp.surf.nl'
GPG=(gpg --homedir "${TMPDIR}")

cat << __EOF__ > "${TMPDIR}"/gpg.conf
quiet
batch
no-tty
no-permission-warning
export-options no-export-attributes,export-clean
keyserver ${KEYSERVER}
keyserver-options no-self-sigs-only
armor
no-emit-version
__EOF__

cd "$(dirname "$0")"

"${GPG[@]}" --gen-key <<EOF
%echo Generating Artix Linux keyring temporary master key...
Key-Type: RSA
Key-Length: 2048
Key-Usage: sign
Name-Real: Artix Linux keyring temporary master key
Name-Email: artix-keyring@localhost
Expire-Date: 0
%no-protection
%commit
%echo Done
EOF

"${GPG[@]}" --import < artix.gpg

rm -rf master{,-revoked} packager{,-revoked} artix-{trusted,revoked}
mkdir master packager master-revoked packager-revoked

# refresh/receive all keys
while read -ra data; do
	keyid="${data[0]}"
	username="${data[@]:1}"
	if "${GPG[@]}" --list-keys ${keyid} >/dev/null &>/dev/null; then
		"${GPG[@]}" --refresh-keys ${keyid} &>/dev/null
	else
		"${GPG[@]}" --recv-keys ${keyid} &>/dev/null
	fi
done < <(cat master-keyids master-revoked-keyids packager-keyids packager-revoked-keyids)

# master-keyids
while read -ra data; do
	keyid="${data[0]}"
	username="${data[@]:1}"
	"${GPG[@]}" --yes --lsign-key ${keyid} &>/dev/null
	"${GPG[@]}" --comment "master-key: ${username} (${keyid})" --export ${keyid} >> master/${username}.asc
	echo "${keyid}:4:" >> artix-trusted
done < master-keyids
"${GPG[@]}" --import-ownertrust < artix-trusted 2>/dev/null

# master-revoked-keyids
while read -ra data; do
	keyid="${data[0]}"
	username="${data[1]}"
	"${GPG[@]}" --comment "revoked master-key: ${username} (${keyid})" --export ${keyid} >> master-revoked/${username}.asc
	echo "${keyid}" >> artix-revoked
done < master-revoked-keyids

# packager-keyids
while read -ra data; do
	keyid="${data[0]}"
	username="${data[@]:1}"
	if ! "${GPG[@]}" --list-keys --with-colons ${keyid} 2>/dev/null | grep -q '^pub:f:'; then
		echo "WARNING: key is not fully trusted: ${keyid} ${username}"
		"${GPG[@]}" --comment "marginal trust: ${username} (${keyid})" --export ${keyid} >> packager/${username}.asc
	else
		"${GPG[@]}" --comment "packager: ${username} (${keyid})" --export ${keyid} >> packager/${username}.asc
	fi
done < packager-keyids

# packager-revoked-keyids
while read -ra data; do
	keyid="${data[0]}"
	username="${data[1]}"
	"${GPG[@]}" --comment "revoked packager: ${username} (${keyid})" --export ${keyid} >> packager-revoked/${username}.asc
	echo "${keyid}" >> artix-revoked
done < packager-revoked-keyids

rm artix.gpg
touch artix.gpg
if [[ $(ls master | wc -l) > 0 ]]; then
	cat master/*.asc >> artix.gpg
fi
if [[ $(ls master-revoked | wc -l) > 0 ]]; then
	cat master-revoked/*.asc >> artix.gpg
fi
if [[ $(ls packager | wc -l) > 0 ]]; then
	cat packager/*.asc >> artix.gpg
fi
if [[ $(ls packager-revoked | wc -l) > 0 ]]; then
	cat packager-revoked/*.asc >> artix.gpg
fi
echo "Keyring created"
